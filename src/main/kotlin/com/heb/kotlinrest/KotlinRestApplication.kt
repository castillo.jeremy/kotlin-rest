package com.heb.kotlinrest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import org.springframework.data.repository.CrudRepository
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.query
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.*
import kotlin.jvm.optionals.toList

// there's still an underlying component scan happening here but since we're in the same file it finds MessageController
@SpringBootApplication
class KotlinRestApplication

fun main(args: Array<String>) {
    runApplication<KotlinRestApplication>(*args)
}

@RestController
class MessageController(val service: MessageService) {
    // a single line expression using string templates. boo! string concatenation
    // the return type (ie String) is not specified because of type inference. boo! NO type inference
    @GetMapping("/")
    fun index(@RequestParam("name") name: String) = "Hello, $name"

    // 'listOf' is a standard function from Kotlin Standard Library (KSL)... this version of it provides a read only list... boo mutability!
    // if you need mutability then the KSL provides this with 'mutableListOf()'
    // these basic implementations also apply to sets & maps
    @GetMapping("/messages")
    fun get() = service.findMessages()

    @GetMapping("/messages/{id}")
    fun getById(@PathVariable id: String): List<Message> = service.findMessagesById(id)

    @PostMapping("/messages")
    fun post(@RequestBody message: Message) = service.save(message)

    @GetMapping("/v2/messages")
    fun getV2() = service.findMessagesV2()

    @GetMapping("/v2/messages/{id}")
    fun getByIdV2(@PathVariable id: String): List<CrudMessage> = service.findMessageByIdV2(id)

    @PostMapping("/v2/messages")
    fun post(@RequestBody message: CrudMessage) = service.saveV2(message)
}

// woah constructor dependency injection without Lombok... boo Lombok!
// the line defining this service 'MessageService(val db: JdbcTemplate)' is defining two things:
// 1. a member field call 'db'
// 2. a primary constructor
// the above two things enable Spring creating the service singleton via the '@Service' annotation
@Service
class MessageService(val db: JdbcTemplate, val messageRepository: MessageRepository) {
    // lambda defined inline as a secondary parameter
    // so the 'query' method in JdbcTemplate actually takes two parameters:
    // 1. a sql string query
    // 2. a 'RowMapper<T>'  which is an interface with the function declaration of 'T mapRow(ResultSet rs, int rowNum) throws SQLException'
    //    KSL recognizes the below syntax because of its focus on functional style coding. in Kotlin if the last parameter is a function
    //    then you can write the callback function outside the method parameter definitions (see other implementations commented)
    fun findMessages(): List<Message> = db.query("select * from msg")
        { row, _ -> Message(row.getString("id"), row.getString("text"))}

    // if you uncomment this line Kotlin's default linting will say to use the 'findMessages' implementation style
    // fun findMessagesWithParametersDefined(): List<Message> = db.query("select * from messages", { row, _ -> Message(row.getString("id"), row.getString("text"))})

    // if you uncomment this Kotlin's default linting will say the 'RowMapper' is redundant because of type inference
    // fun findMessagesWithParametersAndTypesDefined(): List<Message> = db.query("select * from messages", RowMapper { row, _ -> Message(row.getString("id"), row.getString("text"))})

    fun findMessagesById(id: String): List<Message> = db.query("select * from msg where id = ?", id)
        { response, _ -> Message(response.getString("id"), response.getString("text")) }

    fun save(message: Message) {
        // '?:' is called the elvis operator
        // id  = if-message.id-not-null-else randomUUID
        // thank you, thank you very much
        val id = message.id ?: UUID.randomUUID().toString()
        db.update(
            "insert into msg values (?,?)",
            id, message.text
        )
    }

    /* now going to implement the above functionality using spring data */
    fun findMessagesV2(): List<CrudMessage> = messageRepository.findAll().toList()

    fun findMessageByIdV2(id: String): List<CrudMessage> = messageRepository.findById(id).toList()

    fun saveV2(message: CrudMessage) = messageRepository.save(message)

//    fun <T: Any> Optional<out T>.toList(): List<T> = if (isPresent) listOf(get()) else emptyList()
}

// lot going in this one line
// 1. 'data class' are the keywords to define a class in kotlin
// 2. Message(...) is defining member properties on the class and gives us access via a '.id' or '.text'.
// 3. No setter required since we used 'val' for both properties... tldr; val === const | var === let
// 4. Nullable vs non-nullable types... tldr; by default without the '?' a variable is non-nullable
data class Message(val id: String?, val text: String)

@Table("msg")
data class CrudMessage(@Id var id: String?, val text: String)

interface MessageRepository : CrudRepository<CrudMessage, String>
